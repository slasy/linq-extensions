using System;

public static class EnumExtensions
{
    /// <summary>
    /// Parse string to enum value, can ignore case and also can ignore spaces in string
    /// </summary>
    public static T ToEnum<T>(this string value, bool ignoreCase = false, bool ignoreSpaces = false) where T : struct, Enum
    {
        if (ignoreSpaces) value = value.Split(' ').Join("");
        if (Enum.TryParse(value, ignoreCase, out T result)) return result;
        else throw new ArgumentException($"String '{value}' can't be converted to enum of type '{typeof(T)}' - valid values:\n{Enum.GetNames(typeof(T)).ToStringList("\n")}");
    }

    /// <summary>
    /// Same as <see cref="ToEnum{T}(string, bool, bool)"/> but returns <paramref name="defaultValue"/> if string is not valid enum value
    /// </summary>
    public static T ToEnum<T>(this string value, T defaultValue, bool ignoreCase = false, bool ignoreSpaces = false) where T : struct, Enum
    {
        try
        {
            return ToEnum<T>(value, ignoreCase, ignoreSpaces);
        }
        catch (ArgumentException err)
        {
            Console.Error.WriteLine(LogConst.General, $"{err.Message}\n\nfallback to value '{defaultValue}'");
            return defaultValue;
        }
    }

    /// <summary>
    /// Returns all values of given enum
    /// </summary>
    public static T[] GetEnumValues<T>() where T : struct, Enum
    {
        return (T[])Enum.GetValues(typeof(T));
    }
}
